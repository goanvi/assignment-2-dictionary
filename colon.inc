;pointer to the first occurrence (address of the beginning of the dictionary)
%define MARKER 0 

%macro colon 2
    %ifstr %1
        %ifid %2
            %2: 
            dq MARKER
            %define MARKER %2
            db %1, 0
        %else
            %error "Invalid type: expected label"
        %endif
    %else
        %error "Invalid type: expected string"
    %endif            
%endmacro