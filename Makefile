lib.o: lib.asm
	nasm -f elf64 -o $@ $<
dict.o: dict.asm
	nasm -f elf64 -o $@ $<
main.o: main.asm
	nasm -f elf64 -o $@ $<
main: main.o dict.o lib.o
	ld -o $@ $^

.PHONY:
	clean
clean:
	rm *.o
	rm main