section .text
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global read_string
global print_error
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
	xor rdi, rdi
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor	rax, rax
	.loop:
		cmp	byte[rdi + rax], 0
		je	.end
		inc	rax
		jmp	.loop
	.end:
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	call string_length
	pop rdi
	mov	rdx, rax
	mov	rsi, rdi
	mov	rdi, 1
	mov	rax, 1
	syscall
	call print_newline
	ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov	rax, 1
	mov	rsi, rsp
	mov	rdi, 1
	mov	rdx, 1
	syscall
	pop rdi
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov	rdi, 0xA
	call	print_char
	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov	rax, rdi
	mov	r10, 10
	dec rsp
	mov byte[rsp], 0  ;add to stack null term 
	mov rcx, 1 ; rcx - buffer size
	.loop:
		xor rdx, rdx
		div	r10
		inc rcx
		add rdx, '0'
		dec rsp
		mov [rsp], dl
		cmp rax, 0
		jne .loop
	mov rdi, rsp
	push rcx
	call print_string
	pop rcx
	add rsp, rcx
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
	jnl .pos
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
	.pos:
		call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	; word length comparison
	push rdi
	push rsi
	call string_length
	push rax
	mov rdi, rsi
	call string_length
	pop rdx
	pop rsi
	pop rdi
	cmp rax, rdx
	jne .neq
	; word comparison
	mov r8, rax ; r8 - string length 
	cmp r8, 0
	je .eq
	xor rcx, rcx
	.loop:
		mov al, [rdi + rcx]
		mov dl, [rsi + rcx]
		cmp al, dl
 		jne .neq
		inc rcx
		cmp rcx, r8
		je .eq
		jmp .loop
	.eq:
		mov rax, 1
		ret
	.neq:
		mov rax, 0
    	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    dec rsp
    mov rsi, rsp
    mov rdx, 1
    syscall
	cmp rax, 0
	je .result
	mov al, [rsp]
	.result:
		inc rsp
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	mov r9, 0 ; word flag
	mov rdx, 2 ;required buffer size
	.loop:
		; read char
		push rdx
		push rsi
		push rdi
		call read_char
		mov r8, rax
		pop rdi
		pop rsi
		pop rdx
		;check null term
		cmp r8, 0 
		je .nullterm
		;check whitespace
		push rdi 
		mov rdi, r8
		call is_ws
		pop rdi
		cmp rax, 0
		je .is_not_ws
		; check end of word
		cmp r9, 1 
		je .nullterm 
		jmp .loop
		.is_not_ws:
			mov r9, 1
		;check buffer length:
		cmp rdx, rsi
		ja .err
		mov [rdi + rdx - 2], r8 
		inc rdx
		jmp .loop
	.nullterm:
		mov rax, rdi
		mov byte[rdi + rdx - 2], 0 
		dec rdx
		dec rdx
		jmp .end
	.err:
		mov rax, 0
	.end:
    	ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    mov r9, 10
	.loop:
		; character check
    	cmp byte [rdi], '0'
    	jb .end
    	cmp byte [rdi], '9'
    	ja .end
		; hex conversion
    	push rdx
    	mul r9 
    	pop rdx
    	add al, byte [rdi]
    	sub al, '0' ; convert character to number
    	inc rdx
    	inc rdi
    	jmp .loop
	.end:
    	ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	mov al, [rdi]
	cmp al, '-'
	jne .pos
	;negatine number
	inc rdi
	call parse_uint
	cmp rdx, 0
	je	.end
	inc rdx
	neg	rax
	ret
	.pos:
		call parse_uint
	.end:
    	ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rcx, rcx
	xor r8, r8
    push rdi
	push rsi
	push rdx
	call string_length
	pop rdx
	pop rsi
	pop rdi
	inc rax
	cmp rax, rdx
	ja .err
	mov r8, rax ; r8 - string length
	.loop:
		mov rax, [rdi + rcx]
		mov [rsi + rcx], rax
		inc rcx
		cmp rcx, r8
		je .end
		jmp .loop
	.err:
		xor rax, rax
		ret
	.end:
		mov rax, r8	 
    	ret


is_ws:

	cmp rdi, 0x20
	je .ws
	cmp rdi, 0x9
	je .ws
	cmp rdi, 0xa
	je .ws
	xor rax, rax
	ret
	.ws:
		mov al, 1
		ret

read_string:
	mov r9, 0 ; word flag
	mov rdx, 2 ;required buffer size
	.loop:
		; read char
		push rdx
		push rsi
		push rdi
		call read_char
		mov r8, rax
		pop rdi
		pop rsi
		pop rdx
		;check null term
		cmp r8, 0 
		je .nullterm
		;check whitespace
		push rdi 
		mov rdi, r8
		call is_ws
		pop rdi
		cmp rax, 0
		je .is_not_ws
		; check \n
		cmp r8, 10
		je .nullterm
		cmp r9, 1 
		je .is_not_ws 
		jmp .loop
		.is_not_ws:
			mov r9, 1
		;check buffer length:
		cmp rdx, rsi
		ja .err
		mov [rdi + rdx - 2], r8 
		inc rdx
		jmp .loop
	.nullterm:
		mov rax, rdi
		mov byte[rdi + rdx - 2], 0 
		dec rdx
		dec rdx
		jmp .end
	.err:
		mov rax, 0
	.end:
    	ret
		
print_error:
	push rdi
	call string_length
	pop rdi
	mov	rdx, rax
	mov	rsi, rdi
	mov	rdi, 2
	mov	rax, 1
	syscall
	call print_newline
	ret