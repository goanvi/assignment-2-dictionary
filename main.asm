%define BUFFER_SIZE 255

section .data
%include "words.inc"

section .bss
buffer: resb BUFFER_SIZE

section .rodata
out_of_bound: db "string is too long", 0
not_found: db "string not found", 0

section .text
global _start
%include "lib.inc"
_start:
    ;read string
    mov rdi, buffer
    mov rsi, 256 
    push rdi
    call read_string
    pop rdi
    ; if string too long
    cmp rax, 0
    je .out_of_bound
    ;find string in dict 
    mov rsi, MARKER
    call find_word
    ; if string not found
    cmp rax, 0
    je .not_found
    ; get key in object
    add rax, 8
    push rax
    ; count key length
    mov rdi, rax
    call string_length  
    pop rcx
    ;get value in object
    lea rdi, [rcx + rax + 1] 
    ;print value
    call print_string
    call exit
    .not_found:
        mov rdi, not_found
        call print_error
        call exit
    .out_of_bound:
        mov rdi, out_of_bound
        call print_error
        call exit


