%define QWORD 8
section .text
global find_word
extern string_equals
;принимает rdi - pointer to a null-terminated string
;принимает rsi - pointer to dictionary
;rcx - pointer to current entry
;[rcx+QWORD] - entry key
find_word:
    mov rcx, rsi
    .loop:
        lea rsi, [rcx + QWORD]
        push rcx
        push rdi
        call string_equals
        pop rdi
        pop rcx
        cmp rax, 1
        je .return
        cmp qword[rcx], 0
        je .err
        mov rcx, [rcx]
        jmp .loop
    .return:
        mov rax, rcx
        ret
    .err:
        mov rax, 0
        ret


        

